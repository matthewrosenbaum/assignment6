import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, ' \
                                    'year INT UNSIGNED, ' \
                                    'title TEXT,' \
                                    'director TEXT,' \
                                    'actor TEXT,' \
                                    'release_date TEXT,' \
                                    'rating FLOAT,' \
                                    'PRIMARY KEY (id)' \
                                    ')'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def populate_data():

    db, username, password, hostname = get_db_creds()

    print("Inside populate_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                       host=hostname,
                                       database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()
    cur.execute("INSERT INTO movies (year, title, director, actor, release_date, rating) " \
                            "values (2017, 'tit', 'dir', 'act', 'the_date', 9)")
    cnx.commit()
    print("Returning from populate_data")


def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    cur.execute("SELECT title, year, actor, director, release_date, rating FROM movies")
    entries = [dict(title=row[0], year=row[1], actor=row[2], director=row[3], release_date=row[4], rating=row[5]) for row in cur.fetchall()]
    return entries

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table")
    create_table()
    print("After create_table")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/add_movie', methods=['POST'])
def add_movie():
    print("Add movie request received.")
    print(request.form['title'])
    title = request.form['title'].lower()
    year = request.form['year'].lower()
    actor = request.form['actor'].lower()
    director = request.form['director'].lower()
    release_date = request.form['release_date']
    rating = request.form['rating']
    insert_data = (title, year, actor, director, release_date, rating)

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    try:
        cur.execute("INSERT INTO movies (title, year, actor, director, release_date, rating) "\
                            "values ('%s', '%s', '%s', '%s', '%s', '%s')" % insert_data)
        cnx.commit()
        affected_rows = cur.rowcount
        if affected_rows < 1:
            return home(['Movie %s could not be inserted - we\'re not sure what happened' % title])
    except Exception as exp:
            return home(['Movie %s could not be inserted - %s' % (title, str(exp))])
    finally:
        cur.close()
    return home(['Movie %s successfully inserted' % title])

@app.route('/update_movie', methods=['POST'])
def update_movie():
    print("Add movie request received.")
    print(request.form['title'])
    title = request.form['title'].lower()
    year = request.form['year'].lower()
    actor = request.form['actor'].lower()
    director = request.form['director'].lower()
    release_date = request.form['release_date']
    rating = request.form['rating']
    update_data = (title, year, director, actor, release_date, rating, title)

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    try:
        cur.execute("UPDATE movies SET title='%s', year='%s', director='%s', " \
                                    "actor='%s', release_date='%s', rating='%s' " \
                                "WHERE title='%s'" % update_data)
        cnx.commit()
        affected_rows = cur.rowcount
        if affected_rows < 1:
            return home(['Movie %s could not be updated - does not exist, please create a new movie with that title' % title])
    except Exception as exp:
            return home(['Movie %s could not be updated - %s' % (title, str(exp))])
    finally:
        cur.close()
    return home(['Movie %s successfully updated' % title])

@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    print('delete movie request recieved')
    print(request.form['delete_title'])
    delete_title = request.form['delete_title'].lower()

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    try:
        cur.execute("DELETE FROM movies WHERE title='%s'" % delete_title)
        cnx.commit()
        affected_rows = cur.rowcount
        if affected_rows < 1:
            return home(['Movie with %s does not exist' % delete_title])
    except Exception as exp:
            return home(['Movie %s could not be deleted - %s' % (delete_title, str(exp))])
    finally:
        cur.close()
    return home(['Movie %s successfully deleted' % delete_title])

@app.route('/search_movie', methods=['POST'])
def search():
    print('search actor request recieved')
    print(request.form['search_actor'])
    search_actor = request.form['search_actor'].lower()

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    ''' search sql command here '''
    cur.execute("SELECT title, year, actor FROM movies WHERE actor='%s'" % search_actor)
    entries = [(row[0], row[1], row[2]) for row in cur.fetchall()]
    messages = []
    if len(entries) == 0:
        messages += ['No movies found for actor %s' % search_actor]
    else:
        for entry in entries:
            messages += ['\n%s, %s, %s' % entry]
    return home(messages)

def get_stats(stat='MAX'):
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    ''' highest rating sql command here '''
    cur.execute("SELECT title, year, actor, director, rating FROM movies WHERE rating = " \
                "( SELECT %s(rating) FROM movies )" % stat)
    entries = [(row[0], row[1], row[2], row[3], row[4]) for row in cur.fetchall()]
    messages = []
    if len(entries) != 0:
        for entry in entries:
            messages += ['%s, %s, %s, %s, %s' % entry]
    return messages

@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    print('highest_rating request recieved')
    messages = get_stats('MAX')
    return home(messages)

@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    print('highest_rating request recieved')
    messages = get_stats('MIN')
    return home(messages)

@app.route("/")
def home(messages=[]):
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    entries = query_data()
    print("Entries: %s" % entries)
    return render_template('index.html', entries=entries, messages=messages)

if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
